#!/bin/bash

make output/kernel.oex
qemu-system-x86_64 -s -vga vmware -vga std -serial stdio -monitor stdio -no-reboot -m 80 -hda output/disk.img -hdb output/hfsplus.img -rtc base=utc -net nic,model=rtl8139 -net user
