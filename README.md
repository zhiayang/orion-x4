# Orion-X4 #

The fourth rewrite of OrionOS. Designed as a x86_64 OS from the beginning.

> Copyright Orion Industries (zhiayang@gmail.com) © 2011-2014 -- Licensed under CC BY 3.0 (the Apache License Version 2.0)




## Current Features: ##


- Long mode (x86_64)
- BGA Support, works in QEMU, VirtualBox and Bochs.
- Multitasking/Multithreading
- Userspace threads/processes
- Virtual-Address-Space-per-process
- ATA Driver, PIO R/W, DMA RO.
- FAT32 Read-only/Write FS Driver (Write support lacking ATM)
- RTC enabled
- Proper IRQ dispatcher
- Working IPC system.
- Basic networking support (DHCP, DNS, IPv4, TCP/UDP)



## TODO ##

#### Urgent -- in order of importance ####
- Work on userspace console.
- Filesystem mounting (ie. multiple disks/filesystems)


## Detailed Documentation ##

- For detailed documentation, please visit Documentation/Documentation.md






## Changelog ##


##### Everything After That #####
- Check the commit history, they're usually useful.

##### Sunday, 23rd February #####
- IPC system works
- Central Dispatch works
- Keyboard now event (ie. IPC) based.

##### Monday, 20st January #####
- Merged kernelspace and userspace libraries.
- Significantly reduced complexity of things -- since the kernel code will ever only be in once process, the heap as implemented in GlobalHeap should work fine, without all that pointer nonsense.
- Also, syscall vectors 0x3, 0x4 and 0x5 are now deprecated and return zero.
- Userspace FileIO system provides file access to programs.

##### Tuesday, 14th January #####
- VFS Implementation is almost complete, leaving filesystem mounting as the last hurdle.
- Better, more robust IRQ dispatcher.

##### Thursday, 2nd January 2014 #####
- A new year.
- Orion-X4 functionality has officially surpassed that of Orion-X3
- All systems working well.
- Functionality is outlined in the above list.
- New things include parsing ACPI tables.
- Parsing only, nothing is done with them.

##### Thursday, 28th November #####
- Moved into Orion-X4. Below changelog is mostly obsolete, some things not present.
- (eg syscalls, keyboard)
- VAS-per process working well.
- Userspace working well.

##### Saturday, 5th October #####
- Finally finished Ring3 threads, they work pretty much seamlessly the scheduler now.
- Fixed syscalls.

##### Saturday, 14th September #####
- Fixed Ring0->Ring3 and vice-versa transition.
- Added syscalls.

##### Tuesday, 18th June #####
- Added support for adding tasks seamlessly
- X3 now boots from a GRUB1 HDD image

##### Sunday, 14th June #####
- Multitasking support
- Heap structure with expansion (no contraction)

##### Saturday, 7th June #####
- Added float support to printk() -- specify precision *whenever possible*
- Also added SSE support as a by-product of enabling printk()'s float support
- Tweaked the memory test to use memset64(), reduces time taken









## Rantings of Important Concepts ##

##### Thursday, 10th October #####
- Process destuction
	- Add two more fields to Task_type
		- AllocatedPagesStack_Head
		- AllocatedPagesStack_Length
	- As the name implies, we create a stack (allocated in one-page chunks) that houses a list of all the pages that the process has allocated.
	- This would hold 512 allocations (2MB), but if we exceed that amount, then just allocate a new page, increment the length and add that page to the stack itself.
	- The stack would most probably progress *upwards*, not downwards.
	- This is so we can safely delete trailing pages from the stack, since the first allocation (and therefore the last remaining address) would be the last one.
	- When the process is destroyed (either normally or forcibly) the scheduler runs through this stack and cleans up any and all stray pages.
	- Therefore eliminating any memory leaks.
	- Don't forget to free the task structure.



- Virtual Memory
	- When a mapping is requested and does not exist:
		1. Allocate an unmapped page from the PMM.
		2. Create a temporary mapping (note here that structure should all be pre-allocated, if not this doesn't solve anything)
		3. Modify the new page structure as necessary.
		4. Unmap the temporary mapping.
		5. Proceed as normal.

	- When modifying a mapping:
		1. Fetch the physical address of the structure in question.
		2. Proceed from step (2) of the above.





