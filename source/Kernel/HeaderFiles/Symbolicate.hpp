// Symbolicate.hpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.

#include <stdint.h>
#include <String.hpp>

namespace Kernel {
namespace Symbolicate
{
	struct LineNamePair
	{
		uint64_t line;
		Library::string* name;
	};

	void Initialise();
	LineNamePair* ResolveAddress(uint64_t addr);
}
}
