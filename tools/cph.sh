#!/bin/bash

mkdir -p "$2"/usr/include/c++
mkdir -p "$2"/usr/include/orion

# find traditional c includes
find "$1" -name "include" -type d | while read file
do
	rsync -cma "$file"/* "$2"/usr/include
done

# find library includes.
find "$1/Iris/HeaderFiles" -name "*.hpp" -type f | while read file
do
	rsync -ac "$file" "$2"/usr/include/c++/
done

# copy kernel headers
rsync -rRa "source/kernel/HeaderFiles/" "$2"/usr/include/orion/
rsync -arm --remove-source-files "$2/usr/include/orion/source/kernel/HeaderFiles/" "$2/usr/include/orion"
rm -r "$2/usr/include/orion/source"
