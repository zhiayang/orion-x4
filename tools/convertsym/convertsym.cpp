// ConvertSym.cpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.

// quick helper tool to convert 13000+ lines of readelf debug-dump decodedline
// to a binary format, hopefully much easier to parse in-OS.

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include <unordered_map>

using std::string;
struct pair
{
	string cu;
	uint64_t addr;
	uint64_t ln;
};

std::vector<pair> pairs;
std::set<string> cuset;

int main(int argc, char* argv[])
{
	if(argc < 3)
		exit(1);

	// read kernel.sym from ../output/kernel.sym
	std::ifstream input;
	std::ofstream output;
	input.open(argv[1]);
	output.open(argv[2], std::ios::binary | std::ios::out);

	if(!input.is_open())
	{
		printf("Error: output/kernel.sym not found, aborting...\n");
		exit(1);
	}

	if(!output.is_open())
	{
		printf("Error: could not open output/kernel.alm, aborting...\n");
		exit(1);
	}

	int pass = 0;
	string line;
	string cu;

	begin:
	pass++;
	while(std::getline(input, line))
	{
		// printf("[%s]\n", line.c_str());
		if(line.length() == 0 || line.find("Decoded dump of debug contents") == 0)
		{
			continue;
		}
		else if(line.find("CU: ") == 0 || line[line.length() - 1] == ':')
		{
			if(line.find("CU: ") == 0)
				line = line.substr(4);

			// remove colon
			line = line.substr(0, line.length() - 1);

			// try insert.
			if(pass == 1)
				cuset.insert(line);

			cu = line;
		}
		else if(line.find("File name") == 0)
		{
			continue;
		}
		else
		{
			if(pass == 1)
			{
				std::stringstream ss(line);
				string discard;
				uint64_t ln;
				uint64_t addr;
				ss >> discard >> ln >> std::hex >> addr;

				pair* p = new pair;
				p->cu = cu;
				p->ln = ln;
				p->addr = addr;

				pairs.push_back(*p);
			}
		}
	}

	if(pass == 1)
		goto begin;

	// write CUs first.
	uint64_t byteswritten = 0;
	for(string s : cuset)
	{
		output << s.c_str() << '\0';
		byteswritten += s.length() + 1;
		// printf("CU (%ld): %s\n", std::distance(cuset.begin(), cuset.find(s)), s.c_str());
	}
	// write 0xFF to signal end.
	uint8_t end = 0xFF;
	output.write((const char*) &end, sizeof(uint8_t));
	byteswritten++;

	// pad to 32 bytes.
	while(byteswritten % 32 > 0)
	{
		output << '\0';
		byteswritten++;
	}

	for(pair p : pairs)
	{
		// write 16 bytes
		// cu index - line - addr
		uint32_t index = std::distance(cuset.begin(), cuset.find(p.cu));
		output.write((const char*) &index, sizeof(uint32_t));

		uint32_t ln = (uint32_t) p.ln;
		output.write((const char*) &ln, sizeof(uint32_t));
		output.write((const char*) &p.addr, sizeof(uint64_t));
	}

	// write 16 bytes of 0xFF
	uint64_t ff = 0xFFFFFFFFFFFFFFFF;
	output.write((const char*) &ff, sizeof(uint64_t));
	output.write((const char*) &ff, sizeof(uint64_t));

	output.close();
	input.close();
}
