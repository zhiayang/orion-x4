// BitmapLibrary.cpp
// Copyright (c) 2013 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.

#include <math.h>

#include "GraphPlot.hpp"


extern void operator delete(void* p);
extern void operator delete[](void* p);
extern void* operator new(unsigned long size);
extern void* operator new[](unsigned long size);
extern void* operator new(unsigned long, void* addr);


namespace GraphPlot
{
	static void (*DrawPixel)(uint64_t, uint64_t);
	static const long double ddx = 0.000000000000001l;

	void SetPixelPlotter(void (*dp)(uint64_t, uint64_t))
	{
		DrawPixel = dp;
	}


	void DrawYAxisNotch(PixelBoundingBox* dg, uint64_t yval)
	{
		// draw a horizontal line
		DrawPixel(dg->Left + dg->LeftAxO + 0, yval);
		DrawPixel(dg->Left + dg->LeftAxO + 1, yval);
		DrawPixel(dg->Left + dg->LeftAxO + 2, yval);
		DrawPixel(dg->Left + dg->LeftAxO + 3, yval);
		DrawPixel(dg->Left + dg->LeftAxO + 4, yval);
	}


	void DrawXAxisNotch(PixelBoundingBox* dg, uint64_t xval)
	{
		DrawPixel(xval, dg->Top + dg->Height - dg->TopAxO - 5);
		DrawPixel(xval, dg->Top + dg->Height - dg->TopAxO - 4);
		DrawPixel(xval, dg->Top + dg->Height - dg->TopAxO - 3);
		DrawPixel(xval, dg->Top + dg->Height - dg->TopAxO - 2);
		DrawPixel(xval, dg->Top + dg->Height - dg->TopAxO - 1);
	}



	void DrawAxes(PixelBoundingBox* dg, uint64_t scalex, uint64_t scaley)
	{
		// draw the horizontal axis
		for(uint64_t x = dg->Left; x < dg->Left + dg->Length; x++)
			DrawPixel(x, dg->Top + dg->Height - 5 - dg->TopAxO);

		// draw the vertical axis
		for(uint64_t y = dg->Top; y < dg->Top + dg->Height; y++)
			DrawPixel(dg->Left + 5 + dg->LeftAxO, y);



		// draw the y-axis notches
		// start from the centre, go up, then go down.
		for(uint64_t yv = dg->Top + dg->TopAxO - 5; yv < dg->Top + dg->Height + 5; yv += scaley)
			DrawYAxisNotch(dg, yv);

		for(uint64_t yv = dg->Top + dg->TopAxO - 5; yv > dg->Top; yv -= math::min(scaley, yv))
			DrawYAxisNotch(dg, yv);






		// draw the x-axis notches
		// start from the centre, then go left and right.
		for(uint64_t xv = dg->Left + dg->LeftAxO + 5; xv > dg->Left + 5; xv -= math::min(scalex, xv))
			DrawXAxisNotch(dg, xv);

		for(uint64_t xv = dg->Left + dg->LeftAxO + 5; xv < dg->Left + dg->Length + 5; xv += scalex)
			DrawXAxisNotch(dg, xv);
	}








	void PlotGraph2D(Graph2D* graph, GraphBoundingBox* GraphBounds, PixelBoundingBox* DrawingGrounds, uint64_t scalex, uint64_t scaley, long double Granularity, bool DynGran)
	{
		GraphBoundingBox*& gb = GraphBounds;
		PixelBoundingBox*& dg = DrawingGrounds;

		// check if the granularity is too small to be useful on the pixel scale.
		// note that for a circular graph to be completely rendered, the gran must be less than 0.00005.

		if(Granularity * math::max(scalex, scaley) < 1 && DynGran)
			Granularity = 0;

		if(Granularity == 0)
		{
			// if they want the default, then let it be 1/(scale * 2)
			Granularity = 1 / (long double)(math::max(scalex, scaley) * 2);
		}

		uint64_t nx = 0, ny = 0;

		for(long double x = gb->MinX; x < gb->MaxX; x += Granularity)
		{
			long double y = graph->GraphFunction(x, graph);

			uint64_t xv = (uint64_t)((int64_t) dg->Left + (int64_t)(x * scalex) + 5) + dg->LeftAxO;
			uint64_t yv = (uint64_t)((int64_t) dg->Top + (int64_t) dg->Height - (int64_t)(y * scaley) - 5 - (int64_t) dg->TopAxO);


			if(xv > dg->Left + dg->Length || xv < dg->Left || yv > dg->Top + dg->Height || yv < dg->Top)
				continue;

			DrawPixel(xv, yv);

			nx = 0, ny = 0;

			if(DynGran)
			{
				nx = (uint64_t)((int64_t) dg->Left + (int64_t)((x + Granularity) * scalex) + 5) + dg->LeftAxO;
				long double nxy = graph->GraphFunction(x + Granularity, graph);

				ny = (uint64_t)((int64_t) dg->Top + (int64_t) dg->Height - (int64_t)(nxy * scaley) - 5 - (int64_t) dg->TopAxO);

				// calculate the pixel difference
				uint64_t absdx = nx > xv ? nx - xv : xv - nx;
				uint64_t absdy = ny > yv ? ny - yv : yv - ny;

				if(absdx > 1 || absdy > 1)
					Granularity /= 100;
			}


			if(graph->IsAmbiguous)
			{
				y = graph->GraphFunctionAltFunc(x, graph);

				xv = (uint64_t)((int64_t) dg->Left + (int64_t)(x * scalex) + 5) + dg->LeftAxO;
				yv = (uint64_t)((int64_t) dg->Top + (int64_t) dg->Height - (int64_t)(y * scaley) - 5 - (int64_t) dg->TopAxO);


				if(xv > dg->Left + dg->Length || xv < dg->Left || yv > dg->Top + dg->Height || yv < dg->Top)
					continue;

				DrawPixel(xv, yv);
			}

		}
	}


	static long double TangentEqGen(long double x, Graph2D* graph)
	{
		return graph->DGrad * x + graph->DYInt;
	}


	void PlotGraph2DDiff(Graph2D* graph, GraphBoundingBox* GraphBounds, PixelBoundingBox* DrawingGrounds, long double xval, uint64_t scalex, uint64_t scaley)
	{
		GraphBoundingBox*& gb = GraphBounds;
		PixelBoundingBox*& dg = DrawingGrounds;


		// grad at (x, y) = lim dx->0  (f(x + dx) - f(x - dx)) / 2dx
		graph->DGrad = (graph->GraphFunction(xval + ddx, graph) - graph->GraphFunction(xval - ddx, graph)) / (2 * ddx);

		// general form y = mx + c
		// substitute x and y
		graph->DYInt = graph->GraphFunction(xval, graph) - (graph->DGrad * xval);



		for(long double x = gb->MinX; x < gb->MaxX; x += 0.0001)
		{
			long double y = TangentEqGen(x, graph);

			uint64_t xv = (uint64_t)((int64_t) dg->Left + (int64_t)(x * scalex) + 5) + dg->LeftAxO;
			uint64_t yv = (uint64_t)((int64_t) dg->Top + (int64_t) dg->Height - (int64_t)(y * scaley) - 5 - (int64_t) dg->TopAxO);


			if(xv > dg->Left + dg->Length || xv < dg->Left || yv > dg->Top + dg->Height || yv < dg->Top)
				continue;

			DrawPixel(xv, yv);
		}
	}
}







