// BitmapLibrary.hpp
// Copyright (c) 2013 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.


// #include <stdint.h>
#include <string.h>
#include <stdint.h>

#pragma once

namespace GraphPlot
{
	class Graph2D
	{
		public:
			Graph2D(long double (*GraphFunc)(long double, Graph2D*), bool ambiguous = false, long double (*AltFunc)(long double, Graph2D*) = 0)
			{
				// given traditional graphing, GraphFunc should return a y-value when given an x-value.
				this->GraphFunction = GraphFunc;
				// this->ReadableForm = stringrep;
				this->IsAmbiguous = ambiguous;

				if(this->IsAmbiguous && AltFunc)
				{
					this->GraphFunctionAltFunc = AltFunc;
				}
			}

			long double (*GraphFunction)(long double, Graph2D*);
			long double (*GraphFunctionAltFunc)(long double, Graph2D*);
			// Library::string ReadableForm;

			long double DGrad;
			long double DYInt;
			bool IsAmbiguous;
	};

	class PixelBoundingBox
	{
		public:
			PixelBoundingBox(uint64_t left, uint64_t top, uint64_t length, uint64_t height, uint64_t leftaxo = 1, uint64_t topaxo = 1)
			{
				this->Left = left;
				this->Top = top;
				this->Length = length;
				this->Height = height;

				this->LeftAxO = leftaxo;
				this->TopAxO = topaxo;


				if(leftaxo == 1)
					this->LeftAxO = this->Length / 2;

				if(topaxo == 1)
					this->TopAxO = this->Height / 2;
			}

			uint64_t Left, Top, Length, Height, LeftAxO, TopAxO;
	};

	class GraphBoundingBox
	{
		public:
			GraphBoundingBox(long double minx, long double miny, long double maxx, long double maxy)
			{
				this->MinX = minx;
				this->MinY = miny;
				this->MaxX = maxx;
				this->MaxY = maxy;
			}

			long double MinX, MinY, MaxX, MaxY;
	};

	void DrawAxes(PixelBoundingBox* DrawingBounds, uint64_t scalex, uint64_t scaley);
	void SetPixelPlotter(void (*dp)(uint64_t, uint64_t));
	void PlotGraph2D(Graph2D* graph, GraphBoundingBox* GraphBounds, PixelBoundingBox* DrawingGrounds, uint64_t scalex = 10, uint64_t scaley = 10, long double Granularity = 0, bool DynamicGranularity = true);

	void PlotGraph2DDiff(Graph2D* graph, GraphBoundingBox* GraphBounds, PixelBoundingBox* DrawingGrounds, long double x, uint64_t scalex = 10, uint64_t scaley = 10);




	// General usage:
	/*
		Steps:
		1. Create a new graph object. Most of the functions take a Graph2D* (aka pointer to Graph2D).
			Note:
			To handle non-strict-functions (aka ellipses , or where one x gives multiple y), Graph2D can also be initialised with
			two function pointers. Since the plotter does not distinguish between the two, it doesn't matter which one returns a
			positive or negative value -- they are both plotted in the same iteration, with the same x.

			The first function would return one value of y for a given x. In the case of a circle, the positive square root of (r^2 - x^2)
			The second one would return the other value, in this case the negative square root of (r^2 - x^2).



		2. Create two bounding box objects:
			a. PixelBoundingBox. This defines the area in which the graph is allowed to render.
				Parameters: (OffsetFromLeft, OffsetFromTop, Length, Height, [optional: yaxisLeftOffset], [optional: xaxisBottomOffset]
				Note that yaxisoffset and xaxis offset are optional parameters, but their default value is 1.
				A value of 1 will centre the axes, if you choose to draw them -- a value of 0 will make the offset 0.
				Example: to create a bounding box spanning the entire 1024x640 screen, with the x-axis on the bottom and the y-axis
				centred:		PixelBoundingBox(0, 0, 1024, 640, 1, 0)

			b. GraphBoundingBox. This is simpler: it directly defines the range of values that the graph is allowed to have.
				Parameters: (MinX, MinY, MaxX, MaxY). Tabulation of the Graph's points will start at x = MinX until x = MaxX.
				MinY and MaxY are used to determine if the current plotted point would exceed the defined bounding box.

			Note that for both cases, if a point falls outside of the bounding box it will not be rendered.


		3. Set a pixel plotter. This pixel plotter is a function pointer that takes two (2) arguments, namely the x and y coordinates
			of the pixel to plot -- it is assumed that screens count (0, 0) as top-left as opposed to mathematical graphs.

		4. Optionally, draw the axes. This introduces the concept of a 'scale factor', which will be explained later.
			This simply requires the PixelBoundingBox you have created earlier, as it does not involve the graph.

		5. Plot the graph.
			Parameters: (Graph2D*, GraphBoundingBox*, PixelBoundingBox*, ScaleX, ScaleY, Granularity, AdaptiveGranularity)
			This requires a minimum of a Graph2D, GBB and PBB.

			ScaleX and ScaleY form the scaling system -- This is a multiplier applied to every computed value of x and y, to
			turn xy values into pixel coords. Without this scaling system, the graph would be quite small.

			Granularity is essentially StepSize. Because the plotter works by (naively) substituting x-values into the graph
			function, it is necessary to determine how much x will be adjusted every iteration.
			In general, the smaller the value, the smoother the curve, but the more time it will take to render it.

			Finally, AdaptiveGranularity, if enabled, calculates the next point for the next value of x, then scales the
			values up by ScaleX and ScaleY. Its purpose is to ensure a smooth curve rendering -- if the differences
			between the current pixel-xy and the new pixel-xy, the granularity is divided by a certain divisor value.
			Hopefully it would not be too excessive, yet allow the curve to continue plotting smoothly.



		6. Optionally, plot the tangent to the curve at any point. Similar to PlotGraph2D, this takes a Graph2D*, PixelBoundingBox*
			and a GraphBoundingBox*. It also takes a ScaleX and ScaleY values. Most importantly however is this parameter:

			a. (x-value): this determines the value of x at which we want the tangent line. Essentially, this means
				'draw the tangent to the curve at the point (x, y)', where y is calculated by the provided x.

			The preset dx value is 0.000000000000001, which is the smallest for longdouble arithmetic before we get a gradient of zero
			due to rounding errors, or we get NaNs.
	*/

	// Usage Example:

	/*
		GraphPlot::Graph2D* g = new GraphPlot::Graph2D([](long double x){ return math::SquareRoot(16.0l - math::Square(x)); }, string(""), true);

		GraphPlot::PixelBoundingBox* bb = new GraphPlot::PixelBoundingBox(100, 50, 824, 600);
		GraphPlot::GraphBoundingBox* gb = new GraphPlot::GraphBoundingBox(-6, -6, 6, 6);
		GraphPlot::SetPixelPlotter([](uint64_t x, uint64_t y){ LinearFramebuffer::PutPixel((uint16_t) x, (uint16_t) y, 0xFFFFFFFF); });

		GraphPlot::DrawAxes(bb, 40, 40);
		GraphPlot::PlotGraph2D(g, gb, bb, 40, 40, 0.00005, true);
		GraphPlot::PlotGraph2DDiff(g, gb, bb, 3, 0.00001, 40, 40);

	*/

}

















