// StringStream.hpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.

#pragma once
#include <stdint.h>
#include "String.hpp"

namespace Library
{
	class StringStream
	{
		public:
			StringStream();
			StringStream(string& str);
			StringStream(const char* str);

			void write(string& str);
			string* read(char stop = ' ');
			string* readline();

		private:
			int origlength;
			char* backingstore;
	};
}
