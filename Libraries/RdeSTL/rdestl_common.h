#ifndef RDESTL_COMMON_H
#define RDESTL_COMMON_H

#define RDESTL_STANDALONE	1
#include <stdint.h>
#define size_t uint64_t

#define RDE_FORCEINLINE	inline
#define RDE_ASSERT(x)	(x)


void* memset(void* ptr, int value, size_t num);
void* memcpy(void* destination, const void* source, size_t num);
void* memmove(void* destination, const void* source, size_t num);

	namespace rde
	{
		// # Meh. MSVC doesnt seem to have <stdint.h>
		// @todo	Fixes to make this portable.
		typedef unsigned char		uint8_t;
		typedef unsigned short		uint16_t;
		typedef signed long		int32_t;
		typedef unsigned long		uint32_t;
		typedef unsigned long long	uint64_t;

		#define uint8	uint8_t
		#define uint16	uint16_t
		#define uint32	uint32_t
		#define uint64	uint64_t

		#define int8	int8_t
		#define int16	int16_t
		#define int32	int32_t
		#define int64	int64_t

		namespace Sys
		{
			RDE_FORCEINLINE void MemCpy(void* to, const void* from, size_t bytes)
			{
				memcpy(to, from, bytes);
			}
			RDE_FORCEINLINE void MemMove(void* to, const void* from, size_t bytes)
			{
				memmove(to, from, bytes);
			}
			RDE_FORCEINLINE void MemSet(void* buf, unsigned char value, size_t bytes)
			{
				memset(buf, value, bytes);
			}
		} // sys
	}

namespace rde
{
	enum e_noinitialize
	{
		noinitialize
	};
}

#endif // #ifndef RDESTL_H
