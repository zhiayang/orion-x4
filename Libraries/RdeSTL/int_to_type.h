#ifndef RDESTL_INT_TO_TYPE_H
#define RDESTL_INT_TO_TYPE_H

namespace rde
{

template<int TVal>
struct int_to_type
{
    enum
    {
        value = TVal
    };
};

} // namespaces

#endif // #ifndef RDESTL_INT_TO_TYPE_H
