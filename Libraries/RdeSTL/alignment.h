#ifndef RDESTL_ALIGNMENT_H
#define RDESTL_ALIGNMENT_H

#include "rdestl_common.h"
// #include <stddef.h>

namespace rde
{
namespace internal
{
	template<typename T>
	struct alignof_helper
	{
		char	x;
		T		y;
	};

    struct __attribute__ ((aligned (16))) aligned16 { uint64 member[2]; } ;

	template<size_t N> struct type_with_alignment
	{
		typedef char err_invalid_alignment[N > 0 ? -1 : 1];
	};
	template<> struct type_with_alignment<0> {};
	template<> struct type_with_alignment<1> { uint8 member; };
	template<> struct type_with_alignment<2> { uint16 member; };
	template<> struct type_with_alignment<4> { uint32 member; };
	template<> struct type_with_alignment<8> { uint64 member; };
	template<> struct type_with_alignment<16> { aligned16 member; };
}

template<typename T>
struct alignof
{
	enum
	{
		res = offsetof(internal::alignof_helper<T>, y)
	};
};

template<typename T>
struct aligned_as
{
	typedef typename internal::type_with_alignment<alignof<T>::res> res;
};

}

#endif
