// HelloSpamTest.cpp
// Copyright (c) 2013 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under the Apache License Version 2.0.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>

#include <ft2build.h>
#include FT_FREETYPE_H


int main()
{
	FT_Library library;
	FT_Face face;

	printf("Running freetype font test\n");

	int error = FT_Init_FreeType(&library);
	if(error)
	{
		assert("Freetype failed to initialise" && false);
	}


	printf("Reading /menlo.ttf:\n\n");

	FILE* f = fopen("/menlo.ttf", "");
	void* buf = malloc(512);

	auto ret = fread(buf, 1, 500 * 1024, f);
	if(ret == 0)
		assert("file failed to read" && false);

	printf("File read, loading to freetype\n");
	error = FT_New_Memory_Face(library, (FT_Byte*) ret, 500 * 1024, 0, &face);


	if(error == FT_Err_Unknown_File_Format)
	{
		assert("Unknown file format" && false);
	}
	else if(error)
	{
		assert("Other error" && false);
	}

	printf("Font loaded\n");


	return 0;
}









