# Makefile for Orion-X3/Orion-X4 and derivatives
# Written in 2011
# This makefile is licensed under the WTFPL



X_BUILD_FILE	= .build.h

QEMU		= /usr/local/bin/qemu-system-x86_64
BOCHS		= /usr/local/bin/bochs
MKISOFS	= /usr/local/bin/mkisofs

CC		= clang
CXX		= clang++
AS		= Cross64/bin/x86_64-orionx-as
LD		= Cross64/bin/x86_64-orionx-ld
OBJCOPY	= Cross64/bin/x86_64-orionx-objcopy
READELF	= Cross64/bin/x86_64-orionx-readelf
SYSROOT	= ./sysroot
TOOLCHAIN	= ../../Toolchain/orion
GCCVERSION	= 4.9.1

WARNINGS	= -Wno-padded -Wno-c++98-compat-pedantic -Wno-c++98-compat -Wno-cast-align -Wno-unreachable-code -Wno-gnu -Wno-missing-prototypes -Wno-switch-enum -Wno-packed -Wno-missing-noreturn -Wno-float-equal -Wno-sign-conversion -Wno-old-style-cast


CXXFLAGS	= -m64 -Weverything -msse3 -g -integrated-as -O2 -fPIC -std=gnu++11 -nostdinc -ffreestanding -mno-red-zone -fno-stack-protector -fno-exceptions -fno-rtti  -I./source/Kernel/HeaderFiles -I./Libraries/Iris/HeaderFiles -I./Libraries/ -I./sysroot/usr/include -target x86_64-elf -c

LDFLAGS	= --gc-sections -z max-page-size=0x1000 -T link.ld -L$(SYSROOT)/usr/lib


MEMORY	= 1024


SSRC		= $(shell find source -iname "*.s")
CXXSRC	= $(shell find source -iname "*.cpp")

SOBJ		= $(SSRC:.s=.s.o)
CXXOBJ	= $(CXXSRC:.cpp=.cpp.o)

CXXDEPS	= $(CXXOBJ:.o=.d)

.DEFAULT_GOAL = all
-include $(CXXDEPS)




LIBRARIES         = -liris_k -lm -lbitmap
OUTPUT            = output/kernel.oex


.PHONY: builduserspace buildlib mountdisk clean all cleandisk copyheader copylib



all: $(OUTPUT)
	@# unmount??
	@hdiutil detach -quiet /Volumes/Orion-X4

	@echo "# Starting QEMU"
	@$(QEMU) -s -vga std -serial file:"output/serialout.log" -no-reboot -m $(MEMORY) -hda output/disk.img -hdb output/hfsplus.img -rtc base=utc -net nic,model=rtl8139 -net user -net dump,file=output/netdump.wcap
	-@rm -f output/.dmf

	@# mount the disk again for inspection.
	@hdiutil attach -quiet output/disk.img

build: $(OUTPUT)
	# built

$(OUTPUT):  mountdisk copyheader $(SYSROOT)/usr/lib/%.a copylib $(SOBJ) $(CXXOBJ) builduserspace
	@echo "# Linking object files..."
	@$(LD) $(LDFLAGS) -o output/kernel64.elf source/Kernel/Bootstrap/Start.s.o $(shell find source -name "*.o" ! -name "Start.s.o") $(LIBRARIES)


	@echo "# Performing objcopy..."
	@$(OBJCOPY) -O elf32-i386 output/kernel64.elf output/kernel.oex
	@cp $(OUTPUT) /Volumes/Orion-X4/boot/kernel.oex



%.s.o: %.s
	@if [ ! -a output.dmf ]; then tools/updatebuild.sh; fi;
	@touch output/.dmf

	@echo $(notdir $<)
	@$(AS) $< -o $@


%.cpp.o: %.cpp
	@if [ ! -a output.dmf ]; then tools/updatebuild.sh; fi;
	@touch output/.dmf

	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) $(WARNINGS) -MMD -MP -o $@ $<

builduserspace:
	@echo "# Building Userspace library and programs..."
	@make -C userspace/Programs/

copyheader:
	@mkdir -p $(SYSROOT)/usr/lib
	@mkdir -p $(SYSROOT)/usr/include/c++
	@rsync -cmar Libraries/libc/include/* $(SYSROOT)/usr/include/
	@rsync -cmar Libraries/libm/include/* $(SYSROOT)/usr/include/
	@rsync -cmar Libraries/Iris/HeaderFiles/* $(SYSROOT)/usr/include/iris/
	@rsync -cmar Libraries/libsyscall/*.h $(SYSROOT)/usr/include/sys/
	@rsync -cmar $(TOOLCHAIN)/x86_64-orionx/include/c++/$(GCCVERSION)/* $(SYSROOT)/usr/include/c++/

copylib:
	@rsync -cma Libraries/output/* $(SYSROOT)/usr/lib/

buildlib: $(SYSROOT)/usr/lib/%.a
	@:

$(SYSROOT)/usr/lib/%.a:
	@echo "# Building Libraries..."
	@make -C Libraries/

backupdisk:
	@mkdir -p output/backups
	-@rm output/backups/latest.img
	@cp output/disk.img output/backups/latest.img

mountdisk:
	@test -d /Volumes/Orion-X4 || hdiutil attach -quiet output/disk.img

cleandisk:
	@find /Volumes/Orion-X4 -name "*.oex" | xargs rm
	-@rm /Volumes/Orion-X4/hello

clean: cleandisk
	@echo "# Cleaning directory tree..."
	@find source -name "*.o" | xargs rm
	@find Libraries -name "*.o" | xargs rm
	@find Libraries -name "*.a" | xargs rm
	@find userspace -name "*.o" | xargs rm
	-@rm output/*.oex
	-@rm $(CXXDEPS)

