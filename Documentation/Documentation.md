# Documentation for Orion-X4 Subsystems

This file documents the design choices and subsystem functionality of Orion-X4.

Note:
> This file is not a definite reference
> for the implementation and functioning of various functions in the library.
> For a definite implementation specifics,
> Investigate Libraries/Iris/, as well as Global.hpp

### Startup Sequence ###

Orion-X4 starts up in 2 phases.

1. The first phase initialises most low-level subsystems, including:
	- Memory map (from GRUB)
	- Base functions for IRQ and ISR handling
	- Serial logging (separate from console output, typically more verbose) to COM1.
	- The Paging tables (x64)
	- Physical and Dyanmic (heap) memory managers
	- Enumerating of PCI devices (incomplete, but adequate at this stage)
	- BGA (Bochs Graphics Adapter) initialisation for video output
	- Bare-bones multithreading system
	- PS/2 keyboard (starts diabled)
	- Reading the first partition on the first disk to get configuration options from /System/system.cfg
>

2. The next stage is initialised as a thread running on the scheduler; task switches occur at 20Hz, or 20 times a second, thereabouts. Threads therefore get 50ms to operate before preemption. It is managed by the PIT timer. This stage initialises other things:
	- Identify ATA devices (up to 4 max, port I/O values from PCI)
	- Reading of MBR and GPT partitions
	- Initialisation of FAT32 (for now) filesystems.

3. Finally, the boot splash screen is displayed for a short while. Then the userspace console, CarbonShell, is loaded from disk and executed. Keypresses are dispatched through CentralDispatch, soon to be renamed.


### Library Functions ###

Library functions are stored in the namespace Library.
They are further separated into several subtypes:


> #### IrisLibrary
> - basic I/O systems, 
> 
> #### MathLibrary
> - contains C++ wrappers for libm functions. Direct access to libm functions is available, but not recommended for C++ applications.
> 
> #### BitmapLibrary
> - contains a small series of functions designed to parse a .bmp image from memory and render its bitmap to screen.
> 
> #### GraphPlot
> - contains a rather comprehensive set of functions for plotting 2D graphs, including differentiating graphs to obtain the tangent at a point.
> 
> #### UnixCompatLibC
> - includes a mostly-complete, semi-decent implementation of the system-independent portions of libc. This is mostly used to port existing libraries/software to Orion.
> - does not contain libm functions, please link to libm to get math functions. Also note that certain things, like printf or fopen and family are not implemented, because the subsystems to handle those aren't in there.









